<?php
    include_once('dbconnect.php');
    $res = $mysqli->query("SELECT * FROM blog_entries ORDER BY entrydate asc LIMIT 10");
?>
<?php
    echo file_get_contents('head.html');
?>
            <div class="scrollable">
                <?php
                    for ($row_no = $res->num_rows - 1; $row_no >= 0; $row_no--) {
                        $res->data_seek($row_no);
                        $row = $res->fetch_assoc();
                        echo '<div class="container blogpost">';
                        echo "<h3>" . $row['entrytitle'] . "</h3><h5 class='text-muted'>" . $row['entrydate'] . "</h5>";
                        echo "<p>" . $row['entrytext'] . "</p>";
                        echo '</div>';
                    }
                ?>
            </div>
<? echo file_get_contents('footer.html'); ?>

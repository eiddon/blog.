<?php
    include_once('dbconnect.php');
    $res = null;
    if (!empty($_POST)){
        $query = "SELECT * FROM blog_entries WHERE blog_entries.entrytext LIKE '%" . mysqli_real_escape_string($mysqli, $_POST["search"]) . "%'";
        $res = $mysqli->query($query);
    }
?>

<?php
    echo file_get_contents('head.html');
    echo file_get_contents('searchform.html');
?>
<?php
if($res != null && $res->num_rows > 0){
    echo '<div class="scrollable">';
    for ($row_no = $res->num_rows - 1; $row_no >= 0; $row_no--) {
        $res->data_seek($row_no);
        $row = $res->fetch_assoc();
        echo '<div class="container blogpost">';
        echo "<h3>" . $row['entrytitle'] . "</h3><h5 class='text-muted'>" . $row['entrydate'] . "</h5>";
        echo "<p>" . $row['entrytext'] . "</p>";
        echo '</div>';
    }
    echo '</div>';
} else {
    if(!empty($_POST))
        echo "<div class='container'><h3 class='text-warning'>No results found</h3></div>";
}
?>

<?php echo file_get_contents('footer.html'); ?>

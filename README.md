# Blog.

A WIP blogging engine with the aim to be ultra-lite by [Elliot Iddon](http://www.elliotiddon.co.uk). Developed in line with some practical exercises from [CS312 - Web Applications](http://www.strath.ac.uk/cis/localteaching/localug/cs312/) at the University of Strathclyde.

## Install

Currently extremely manual, not reccomended for a production blog, some effort has been made to prevent SQL injection but it's my no means a guarantee, if you care about your database you should restrict the access that the Blog user has.

`CREATE TABLE blog_entries (entrydate TIMESTAMP PRIMARY KEY, entrytitle VARCHAR(100), entrytext TEXT);`

`CREATE TABLE blog_user (email VARCHAR(32) PRIMARY KEY, name VARCHAR(32), password VARCHAR(32));`

To create the tables.

`INSERT INTO blog_user values("your.mail@example.com", "your.name", "password"`

Obviously you need to fill in some of your own information here, the password isn't encrypted or hashed anywhere at present so don't use anything you mind anyone knowing. (Serverside hash of the password will be implemented in the near future as well as insistense that https:// is used for any sessions.)

Create a config.php, copy config.php.example and edit the fields to have the correct values.

## TODO

+ Security.
+ Install procedure.
+ ~~Better~~ Admin Console.
+ Extensibility.
+ Mark Up for posts.
+ Post editing.
+ Themeing.

